import re
from collections import defaultdict
from typing import Dict, List, Tuple

from icecream import ic


def strip_differing_char(differs_by_one: Tuple[str, str]) -> str:
    one, two = differs_by_one
    result = ''
    for i in range(len(one)):
        if one[i] == two[i]:
            result += one[i]
    return result


def parse_inputs(inputs: List[str]) -> dict:
    res = {}
    for item in inputs:
        parsed = re.match(r'#(\d+) @ (\d+),(\d+): (\d+)x(\d+)', item)
        c_id, from_left, from_top, size_width, size_height = parsed.groups()
        res[c_id] = {
            'left': from_left,
            'top': from_top,
            'w': size_width,
            'h': size_height,
        }
    return res


def claim_areas(claims: dict) -> dict:
    def make_dict():
        return {'count': 0, 'areas': set()}
    areas = defaultdict(make_dict)
    for cl_id, claim in claims.items():
        left = int(claim['left'])
        top = int(claim['top'])
        for w in range(int(claim['w'])):
            for h in range(int(claim['h'])):
                key = (left + w, top + h)
                areas[key]['count'] += 1
                areas[key]['areas'].add(cl_id)

    return areas


def get_nonoverlapping(areas: Dict[Tuple[int, int], dict]) -> int:
    area_counts = defaultdict(set)
    for patch_coordinate, area_info in areas.items():
        for area_id in area_info['areas']:
            area_counts[area_id] |= area_info['areas']
    for area, areas in area_counts.items():
        if len(areas) == 1:
            return int(area)


def process(inputs: List[str]):
    claims = parse_inputs(inputs)
    areas = claim_areas(claims)
    overlaps = get_nonoverlapping(areas)
    return overlaps


def main():
    with open('2.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print(result)


if __name__ == '__main__':
    main()


def test_1():
    case = """#1 @ 1,3: 4x4
#2 @ 3,1: 4x4
#3 @ 5,5: 2x2"""
    inputs = case.split('\n')
    res = process(inputs)
    assert res == 3


