from collections import Counter
from typing import List, Tuple


def strings_with_two_and_three_same_letters(inputs: List[str]) -> List[str]:
    twos = 0
    threes = 0
    strings = set()
    for item in inputs:
        c = Counter(item)
        if 2 in c.values():
            twos += 1
            strings.add(item)
        if 3 in c.values():
            threes += 1
            strings.add(item)
    return strings


def find_differing_by_one_char(correct_strings: List[str]) -> List[str]:
    analyzed = []
    for one in correct_strings:
        for two in correct_strings:
            diff = 0
            for i in range(len(one)):
                if one[i] != two[i]:
                    diff += 1
                    if diff > 1:
                        continue
            if diff == 1:
                analyzed.append([one, two])
    return analyzed[0]


def strip_differing_char(differs_by_one: Tuple[str, str]) -> str:
    one, two = differs_by_one
    result = ''
    for i in range(len(one)):
        if one[i] == two[i]:
            result += one[i]
    return result


def process(inputs: List[str]):
    correct_strings = strings_with_two_and_three_same_letters(inputs)
    differs_by_one = find_differing_by_one_char(correct_strings)
    result = strip_differing_char(differs_by_one)
    return result


def main():
    with open('2.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print(result)


if __name__ == '__main__':
    main()
