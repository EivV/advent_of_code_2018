from collections import Counter
from typing import List


def process(inputs: List[str]):
    twos = 0
    threes = 0
    for item in inputs:
        c = Counter(item)
        if 2 in c.values():
            twos += 1
        if 3 in c.values():
            threes += 1
    return twos * threes


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print(result)


if __name__ == '__main__':
    main()
