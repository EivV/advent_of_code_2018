import datetime
import re
from collections import Counter, defaultdict
from dataclasses import dataclass, field
from operator import itemgetter
from typing import Dict, List, Tuple, Iterable, Set

from icecream import ic


@dataclass
class Point:
    name: str
    prerequisites: Set['Point'] = field(default_factory=set)
    completed: bool = False

    def __hash__(self):
        return hash(self.name)

    def __repr__(self):
        return (
            f"{self.completed}+{self.name}#{self.__hash__()}, "
            f"pre: {self.prerequisites}"
        )


_all_points: Dict[str, Point] = {}


@dataclass
class PointsBox:
    points: Dict[str, Point] = field(default_factory=dict)

    def get_point(self, name: str) -> Point:
        try:
            point = self.points[name]
        except KeyError:
            self.add_point_by_name(name)
            point = self.points[name]
        return point

    def add_point(self, name: str, prerequisites: List[str] = None) -> None:
        point = self.get_point(name)
        if prerequisites:
            for pre in prerequisites:
                pre_point = self.get_point(pre)
                point.prerequisites.add(pre_point)

    def add_point_by_name(self, name: str) -> None:
        if name not in self.points:
            self.points[name] = Point(name=name)


def parse_inputs(inputs: List[str]) -> PointsBox:
    points_box = PointsBox()
    for item in inputs:
        parsed = re.match(
            r'Step (\w) must be finished before step (\w) can begin.',
            item,
        )
        prerequisite, name = parsed.groups()
        points_box.add_point(name=prerequisite)
        points_box.add_point(name=name, prerequisites=[prerequisite])
    return points_box


def process_points(box: PointsBox) -> str:
    res = ''
    while any(not p.completed for p in box.points.values()):
        for point_name, point in sorted(box.points.items(), key=itemgetter(0)):
            for pre in point.prerequisites:
                if not pre.completed:
                    break
            else:
                point.completed = True
                if point.name not in res:
                    res += point.name
                    break
    return res


def process(inputs: List[str]):
    points = parse_inputs(inputs)
    result = process_points(points)
    return result


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print(result)


if __name__ == '__main__':
    main()


def test_1():
    """
      -->A--->B--
     /    \      \
    C      -->D----->E
     \           /
      ---->F-----
    """
    case = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""
    inputs = case.split('\n')
    res = process(inputs)
    assert res == 'CABDFE'


def test_2():
    main()
