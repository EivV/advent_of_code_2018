import datetime
import re
import string
from collections import Counter, defaultdict
from dataclasses import dataclass, field
from operator import itemgetter
from typing import Dict, List, Tuple, Iterable, Set, Optional

from icecream import ic


def _get_in_progress(item: tuple) -> (int, str):
    name, point = item
    return (-point.ticks_done), name


def get_durations(base: int = 61) -> Dict[str, int]:
    return {
        letter.upper(): dur + base
        for dur, letter
        in enumerate(string.ascii_lowercase)
    }


@dataclass
class Point:
    name: str
    duration: int
    prerequisites: Set['Point'] = field(default_factory=set)
    completed: bool = False
    ticks_done: int = 0

    def __hash__(self):
        return hash(self.name)

    def __repr__(self):
        return (
            f"{self.completed and '+' or '-'}{self.name}"
            f"[{self.ticks_done}/{self.duration}], "
            f"pre: {[(p.completed, p.name) for p in self.prerequisites]}"
        )

    def tick(self) -> bool:
        if not self.completed:
            self.ticks_done += 1
            if self.ticks_done == self.duration:
                self.completed = True
                return True
        else:
            raise RuntimeError("Cannot tick, already completed.")
        return False


_all_points: Dict[str, Point] = {}


@dataclass
class PointsBox:
    durations: Dict[str, int]
    points: Dict[str, Point] = field(default_factory=dict)

    def get_point(self, name: str) -> Point:
        try:
            point = self.points[name]
        except KeyError:
            self.add_point_by_name(name)
            point = self.points[name]
        return point

    def add_point(self, name: str, prerequisites: List[str] = None) -> None:
        point = self.get_point(name)
        if prerequisites:
            for pre in prerequisites:
                pre_point = self.get_point(pre)
                point.prerequisites.add(pre_point)

    def add_point_by_name(self, name: str) -> None:
        if name not in self.points:
            duration = self.durations[name]
            self.points[name] = Point(name=name, duration=duration)

    def get_available_not_completed_points(self) -> List[Point]:
        res = []
        for point_name, point in sorted(
                self.points.items(),
                key=_get_in_progress,
        ):
            for pre in point.prerequisites:
                if not pre.completed:
                    break
            else:
                if not point.completed:
                    res.append(point)
        return res

    def complete_tick(self, workers: int = 1) -> bool:
        points_to_do = self.get_available_not_completed_points()
        # log = ''
        for point in points_to_do[:workers]:
            # log += point.name
            point.tick()
        # log = log.ljust(2, '.')
        # print(log)
        work_has_been_done = bool(points_to_do)
        return work_has_been_done


def parse_inputs(inputs: List[str], durations: Dict[str, int]) -> PointsBox:
    points_box = PointsBox(durations=durations)
    for item in inputs:
        parsed = re.match(
            r'Step (\w) must be finished before step (\w) can begin.',
            item,
        )
        prerequisite, name = parsed.groups()
        points_box.add_point(name=prerequisite)
        points_box.add_point(name=name, prerequisites=[prerequisite])
    return points_box


def process_points(box: PointsBox) -> str:
    res = ''
    while any(not p.completed for p in box.points.values()):
        for point_name, point in sorted(box.points.items(), key=itemgetter(0)):
            for pre in point.prerequisites:
                if not pre.completed:
                    break
            else:
                point.completed = True
                if point.name not in res:
                    res += point.name
                    break
    return res


def process_box(box: PointsBox, workers: int = 5) -> int:
    ticks = -1  # Reports, if done - add one for no work completed.
    there_is_work = True
    while there_is_work:
        there_is_work = box.complete_tick(workers=workers)
        ticks += 1
    return ticks


def process(inputs: List[str], base: int = 61, workers: int = 5):
    durations = get_durations(base)
    box = parse_inputs(inputs, durations=durations)
    result = process_box(box, workers=workers)
    return result


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1], base=61, workers=5)
    print(result)


if __name__ == '__main__':
    main()


def test_1():
    r"""
      -->A--->B--
     /    \      \
    C      -->D----->E
     \           /
      ---->F-----
    """
    case = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""
    inputs = case.split('\n')
    res = process(inputs, base=1, workers=2)
    assert res == 15


def test_2():
    main()
