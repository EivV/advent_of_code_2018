

def combine_opposites(polymer: str) -> str:
    cursor = 0
    # Break before reaching last, included in the algorithm.
    while cursor < len(polymer)-2:
        this, next = polymer[cursor:cursor+2]
        if this.lower() == next.lower() and this != next:
            polymer = polymer[:cursor] + polymer[cursor+2:]
            cursor = cursor - 1 if cursor else 0
        else:
            cursor += 1
    return polymer


def process(polymer: str) -> int:
    result = combine_opposites(polymer)
    return len(result)


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[0])  # Only one line this time.
    print(f"Result: {result}")


if __name__ == '__main__':
    main()


def test_1():
    case = 'dabAcCaCBAcCcaDA'
    res = process(case)
    assert res == 10

