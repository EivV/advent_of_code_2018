import re
from typing import Set, Dict, Tuple


def get_elements(polymer: str) -> set:
    return set(polymer.lower())


def combine_opposites(polymer: str) -> str:
    cursor = 0
    # Break before reaching last, included in the algorithm.
    while cursor < len(polymer)-2:
        this, next = polymer[cursor:cursor+2]
        if this.lower() == next.lower() and this != next:
            polymer = polymer[:cursor] + polymer[cursor+2:]
            cursor = cursor - 1 if cursor else 0
        else:
            cursor += 1
    return polymer


def polarize_element(element: str) -> str:
    return element.lower() + element.upper()


def strip_polymer(polymer: str, element: str) -> str:
    result = polymer
    for polarity in element:
        result = re.sub(polarity, '', result)
    return result


def get_reactions(polymer: str, elements: Set[str]) -> Dict[str, str]:
    res = {}
    for element in elements:
        polarized_element = polarize_element(element)
        stripped_polymer = strip_polymer(polymer, polarized_element)
        reaction_result = combine_opposites(stripped_polymer)
        res[element] = reaction_result
    return res


def _len_of_val(tuple_):
    return len(tuple_[1])


def get_shortest_pair(
        reactions_without_an_element: Dict[str, str],
) -> Tuple[str, str]:
    return min(reactions_without_an_element.items(), key=_len_of_val)


def process(polymer: str) -> int:
    elements = get_elements(polymer)
    reactions_without_an_element = get_reactions(polymer, elements)
    culprit_element, shortest_polymer = get_shortest_pair(
        reactions_without_an_element
    )
    return len(shortest_polymer)


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[0])  # Only one line this time.
    print(f"Result: {result}")


if __name__ == '__main__':
    main()


def test_1():
    case = 'dabAcCaCBAcCcaDA'
    res = process(case)
    assert res == 4

