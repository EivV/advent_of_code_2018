from part_2 import process


inputs = r"""Before: [3, 2, 1, 1]
9 2 1 2
After:  [3, 2, 2, 1]
"""

actions = r"""15 0 1 1
2 0 2 0
5 0 3 0
13 0 3 3"""


def test_example_1():
    res = process(inputs.split('\n'), actions.split('\n'))
    assert res == 1
