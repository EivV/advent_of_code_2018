import datetime
import re
from collections import Counter, defaultdict
from dataclasses import dataclass, field
from operator import itemgetter
from typing import Dict, List, Tuple, Iterable, Set, Union

from icecream import ic


def _add(a: int, b: int) -> int:
    return a + b


def _mul(a: int, b: int) -> int:
    return a * b


def _ban(a: int, b: int) -> int:
    return a & b


def _bor(a: int, b: int) -> int:
    return a | b


def _set(a: int, b: int) -> int:
    return a


def _gt(a: int, b: int) -> int:
    return 1 if a > b else 0


def _eq(a: int, b: int) -> int:
    return 1 if a == b else 0


class Processor:

    def __init__(self):
        self._r: List[int] = [0] * 4
        self.ops: Set[callable] = {
            self.addr,
            self.addi,
            self.mulr,
            self.muli,
            self.banr,
            self.bani,
            self.borr,
            self.bori,
            self.setr,
            self.seti,
            self.gtir,
            self.gtri,
            self.gtrr,
            self.eqir,
            self.eqri,
            self.eqrr,
        }

    @property
    def registers(self):
        return self._r

    # addr (add register) stores into register C the result of
    # adding register A and register B.
    def addr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _add(self._r[a], self._r[b])

    # addi (add immediate) stores into register C the result of
    # adding register A and value B.
    def addi(self, a: int, b: int, c: int) -> None:
        self._r[c] = _add(self._r[a], b)

    # mulr (multiply register) stores into register C the result of
    # multiplying register A and register B.
    def mulr(self, a: int, b: int, c: int) -> None:
        # self._r[c] = self.a[a] * self.b[b]
        self._r[c] = _mul(self._r[a], self._r[b])

    # muli (multiply immediate) stores into register C the result of
    # multiplying register A and value B.
    def muli(self, a: int, b: int, c: int) -> None:
        # self._r[c] = self.a[a] * b
        self._r[c] = _mul(self._r[a], b)

    # banr (bitwise AND register)
    # stores into register C the result of the bitwise AND of register A and register B.
    def banr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _ban(self._r[a], self._r[b])

    # bani (bitwise AND immediate)
    # stores into register C the result of the bitwise AND of register A and value B.
    def bani(self, a: int, b: int, c: int) -> None:
        self._r[c] = _ban(self._r[a], b)

    # borr (bitwise OR register)
    # stores into register C the result of the bitwise OR of register A and register B.
    def borr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _bor(self._r[a], self._r[b])

    # bori (bitwise OR immediate)
    # stores into register C the result of the bitwise OR of register A and value B.
    def bori(self, a: int, b: int, c: int) -> None:
        self._r[c] = _bor(self._r[a], b)

    # setr (set register)
    # copies the contents of register A into register C. (Input B is ignored.)
    def setr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _set(self._r[a], self._r[b])

    # seti (set immediate)
    # stores value A into register C. (Input B is ignored.)
    def seti(self, a: int, b: int, c: int) -> None:
        self._r[c] = _set(a, b)
    # gtir (greater-than immediate/register)
    # sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
    def gtir(self, a: int, b: int, c: int) -> None:
        self._r[c] = _gt(a, self._r[b])

    # gtri (greater-than register/immediate)
    # sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
    def gtri(self, a: int, b: int, c: int) -> None:
        self._r[c] = _gt(self._r[a], b)

    # gtrr (greater-than register/register)
    # sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
    def gtrr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _gt(self._r[a], self._r[b])

    # eqir (equal immediate/register)
    # sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
    def eqir(self, a: int, b: int, c: int) -> None:
        self._r[c] = _eq(a, self._r[b])

    # eqri (equal register/immediate)
    # sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
    def eqri(self, a: int, b: int, c: int) -> None:
        self._r[c] = _eq(self._r[a], b)

    # eqrr (equal register/register)
    # sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
    def eqrr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _eq(self._r[a], self._r[b])


class SearchingProcessor(Processor):

    def __init__(
            self,
            initial_registers: List[int],
            looking_for: List[int],
            action: List[int],
    ) -> None:

        super().__init__()
        self.initial_registers = initial_registers
        self.looking_for = looking_for
        self.action = action

    def get_applicable_ops(self):
        ops_applicable = 0
        for op in self.ops:
            self._r = self.initial_registers[:]
            op_id, a, b, c = self.action
            op(a, b, c)
            if self._r == self.looking_for:
                ops_applicable += 1
        return ops_applicable


def parse_inputs(inputs: List[str]) -> List[dict]:
    res = []
    for i in range(0, len(inputs), 4):
        before, values, after, *_ = inputs[i:i+4]
        before_list_re = list(map(int, re.findall(r'\d', before)))
        after_list_re = list(map(int, re.findall(r'\d', after)))
        res.append({
            'before': before_list_re,
            'values': list(map(int, values.split(' '))),
            'after': after_list_re,
        })
    return res


def process(inputs: List[str]):
    parsed = parse_inputs(inputs)
    ic(parsed)
    result = 0
    for item in parsed:
        p = SearchingProcessor(
            initial_registers=item['before'],
            looking_for=item['after'],
            action=item['values'],
        )
        applicable_ops = p.get_applicable_ops()
        if applicable_ops >= 3:
            result += 1
    return result


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print(result)


if __name__ == '__main__':
    main()
