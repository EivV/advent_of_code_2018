from part_1 import process


inputs = r"""Before: [3, 2, 1, 1]
9 2 1 2
After:  [3, 2, 2, 1]
"""


def test_example_1():
    res = process(inputs.split('\n'))
    assert res == 1
