import datetime
import re
from collections import Counter, defaultdict
from dataclasses import dataclass, field
from operator import itemgetter
from typing import Dict, List, Tuple, Iterable, Set, Union

from icecream import ic


def _add(a: int, b: int) -> int:
    return a + b


def _mul(a: int, b: int) -> int:
    return a * b


def _ban(a: int, b: int) -> int:
    return a & b


def _bor(a: int, b: int) -> int:
    return a | b


def _set(a: int, b: int) -> int:
    return a


def _gt(a: int, b: int) -> int:
    return 1 if a > b else 0


def _eq(a: int, b: int) -> int:
    return 1 if a == b else 0


class Processor:

    def __init__(self, actions_map: Dict[int, str] = None):
        self._r: List[int] = [0] * 4
        self.ops: Dict[callable] = {
            'addr': self.addr,
            'addi': self.addi,
            'mulr': self.mulr,
            'muli': self.muli,
            'banr': self.banr,
            'bani': self.bani,
            'borr': self.borr,
            'bori': self.bori,
            'setr': self.setr,
            'seti': self.seti,
            'gtir': self.gtir,
            'gtri': self.gtri,
            'gtrr': self.gtrr,
            'eqir': self.eqir,
            'eqri': self.eqri,
            'eqrr': self.eqrr,
        }
        if actions_map is None:
            actions_map = {}
        self.actions_map = actions_map

    @property
    def registers(self):
        return self._r

    # addr (add register) stores into register C the result of
    # adding register A and register B.
    def addr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _add(self._r[a], self._r[b])

    # addi (add immediate) stores into register C the result of
    # adding register A and value B.
    def addi(self, a: int, b: int, c: int) -> None:
        self._r[c] = _add(self._r[a], b)

    # mulr (multiply register) stores into register C the result of
    # multiplying register A and register B.
    def mulr(self, a: int, b: int, c: int) -> None:
        # self._r[c] = self.a[a] * self.b[b]
        self._r[c] = _mul(self._r[a], self._r[b])

    # muli (multiply immediate) stores into register C the result of
    # multiplying register A and value B.
    def muli(self, a: int, b: int, c: int) -> None:
        # self._r[c] = self.a[a] * b
        self._r[c] = _mul(self._r[a], b)

    # banr (bitwise AND register)
    # stores into register C the result of the bitwise AND of register A and register B.
    def banr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _ban(self._r[a], self._r[b])

    # bani (bitwise AND immediate)
    # stores into register C the result of the bitwise AND of register A and value B.
    def bani(self, a: int, b: int, c: int) -> None:
        self._r[c] = _ban(self._r[a], b)

    # borr (bitwise OR register)
    # stores into register C the result of the bitwise OR of register A and register B.
    def borr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _bor(self._r[a], self._r[b])

    # bori (bitwise OR immediate)
    # stores into register C the result of the bitwise OR of register A and value B.
    def bori(self, a: int, b: int, c: int) -> None:
        self._r[c] = _bor(self._r[a], b)

    # setr (set register)
    # copies the contents of register A into register C. (Input B is ignored.)
    def setr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _set(self._r[a], self._r[b])

    # seti (set immediate)
    # stores value A into register C. (Input B is ignored.)
    def seti(self, a: int, b: int, c: int) -> None:
        self._r[c] = _set(a, b)
    # gtir (greater-than immediate/register)
    # sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
    def gtir(self, a: int, b: int, c: int) -> None:
        self._r[c] = _gt(a, self._r[b])

    # gtri (greater-than register/immediate)
    # sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
    def gtri(self, a: int, b: int, c: int) -> None:
        self._r[c] = _gt(self._r[a], b)

    # gtrr (greater-than register/register)
    # sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
    def gtrr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _gt(self._r[a], self._r[b])

    # eqir (equal immediate/register)
    # sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0.
    def eqir(self, a: int, b: int, c: int) -> None:
        self._r[c] = _eq(a, self._r[b])

    # eqri (equal register/immediate)
    # sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
    def eqri(self, a: int, b: int, c: int) -> None:
        self._r[c] = _eq(self._r[a], b)

    # eqrr (equal register/register)
    # sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
    def eqrr(self, a: int, b: int, c: int) -> None:
        self._r[c] = _eq(self._r[a], self._r[b])

    def process(self, action: List[int]) -> None:
        op_id, a, b, c = action
        getattr(self, self.actions_map[op_id])(a, b, c)

    def process_actions(self, actions: List[List[int]]) -> None:
        for action in actions:
            self.process(action)

class SearchingProcessor(Processor):

    def __init__(
            self,
            initial_registers: List[int],
            looking_for: List[int],
            action: List[int],
    ) -> None:

        super().__init__()
        self.initial_registers = initial_registers
        self.looking_for = looking_for
        self.action = action

    def get_applicable_ops(self) -> Tuple[int, List[callable]]:
        ops_applicable = set()
        op_id, a, b, c = self.action
        for op_name, op in self.ops.items():
            self._r = self.initial_registers[:]
            op(a, b, c)
            if self._r == self.looking_for:
                ops_applicable.add(op_name)
        return op_id, list(ops_applicable)


def parse_inputs(inputs: List[str]) -> List[dict]:
    res = []
    for i in range(0, len(inputs), 4):
        before, values, after, *_ = inputs[i:i+4]
        before_list_re = list(map(int, re.findall(r'\d', before)))
        after_list_re = list(map(int, re.findall(r'\d', after)))
        res.append({
            'before': before_list_re,
            'values': list(map(int, values.split(' '))),
            'after': after_list_re,
        })
    return res


def parse_actions(actions: List[str]) -> List[List[int]]:
    return [list(map(int, i.split(' '))) for i in actions]


def map_ops_to_op_ids(hints: Dict[int, List[Set[callable]]]) -> Dict[int, str]:
    result = {}
    partial = None
    for op_id, op_hints_list in hints.items():
        for hint_set in op_hints_list:
            if partial is not None:
                partial &= set(hint_set)
            else:
                partial = set(hint_set)
        result[op_id] = partial
        partial = None

    while any(len(ops) != 1 for ops in result.values()):
        for op_id, ops in result.items():
            if len(ops) == 1:
                for op_id_, ops_set in result.items():
                    if op_id != op_id_:
                        ops_set -= ops
    return dict((op_id, list(ops)[0]) for op_id, ops in result.items())


def process(inputs: List[str], actions_in: List[str]):
    parsed = parse_inputs(inputs)
    actions = parse_actions(actions_in)
    hints = defaultdict(list)
    for item in parsed:
        p = SearchingProcessor(
            initial_registers=item['before'],
            looking_for=item['after'],
            action=item['values'],
        )
        applicable_ops = p.get_applicable_ops()
        op_id, ops = applicable_ops
        hints[op_id].append(ops)
    ops_map = map_ops_to_op_ids(hints)

    processor = Processor(actions_map=ops_map)
    processor.process_actions(actions)
    result = processor.registers[0]
    return result


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')
    with open('2.txt', 'r') as f:
        file = f.read()
    actions = file.split('\n')

    result = process(inputs[:-1], actions_in=actions[:-1])
    print(result)


if __name__ == '__main__':
    main()
