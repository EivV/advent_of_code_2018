from typing import List


def find_repeating_frequency(changes: List[str]):
    freq = 0
    doubled = False
    seen_freqs = {0: 1}
    while not doubled:
        for change in changes:
            op = change[0]
            if op == '-':
                freq -= int(change[1:])
            else:
                freq += int(change[1:])
            if freq not in seen_freqs:
                seen_freqs[freq] = 1
            else:
                return freq


def main():
    with open('2.txt', 'r') as f:
        file = f.read()
    changes = file.split('\n')

    # Last line is empty.
    repeating_freq = find_repeating_frequency(changes[:-1])
    print(repeating_freq)


if __name__ == '__main__':
    main()


def test_1():
    changes = '+1 -1'.split(' ')
    res = find_repeating_frequency(changes)
    exp = 0
    assert res == exp


def test_2():
    changes = '+3 +3 +4 -2 -4'.split(' ')
    res = find_repeating_frequency(changes)
    exp = 10
    assert res == exp
