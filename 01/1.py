

def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    changes = file.split('\n')

    start = 0
    for change in changes[:-1]:  # Last line is empty.
        op = change[0]
        if op == '-':
            start -= int(change[1:])
        else:
            start += int(change[1:])
    print(start)


if __name__ == '__main__':
    main()
