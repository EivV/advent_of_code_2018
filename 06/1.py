import datetime
import re
import string
from collections import Counter, defaultdict
from dataclasses import dataclass, field
from itertools import cycle
from operator import itemgetter
from typing import Dict, List, Tuple, Iterable, Set

from scipy.spatial.distance import cityblock

from icecream import ic


@dataclass
class BlockedSide:
    left: int = 0
    right: int = 0
    top: int = 0
    bottom: int = 0
    center: int = 0

    def get_side(self, side: str) -> int:
        return getattr(self, side)

    def __bool__(self) -> bool:
        return any((self.left, self.right, self.top, self.bottom))


@dataclass
class Bound:
    left: int
    right: int
    top: int
    bottom: int


@dataclass
class PointDiff:
    x: int
    y: int


@dataclass
class Point:
    x: int
    y: int
    name: str = '~'
    owner: 'Point' = None
    is_infinite: bool = False
    distance: int = 0
    blocked_sides: BlockedSide = BlockedSide()

    # def get_side_from(self, center: 'Point') -> Side:
    #     if self.x < center.x:
    #         x = 'left'
    #     elif self.x == center.x:
    #         x = 'center'
    #     else:
    #         x = 'right'
    #     if self.y < center.y:
    #         y = 'bottom'
    #     elif self.y == center.y:
    #         y = 'center'
    #     else:
    #         y = 'top'
    #     return Side(x, y)

    def __sub__(self, other: 'Point') -> PointDiff:
        return PointDiff(x=self.x-other.x, y=self.y-other.y)

    # FIXME: used?
    def is_blocked_on_both_sides(self, diff: PointDiff) -> bool:
        side_x = self._get_side_x(diff.x)
        if abs(self.blocked_sides.get_side(side_x)) > abs(diff.x):
            side_y = self._get_side_y(diff.y)
            if abs(self.blocked_sides.get_side(side_y)) > abs(diff.x):
                return True
        return False

    @staticmethod
    def _get_side_x(diff: int) -> str:
        if diff < 0:
            return 'left'
        elif diff > 0:
            return 'right'
        return 'center'

    @staticmethod
    def _get_side_y(diff: int) -> str:
        if diff < 0:
            return 'bottom'
        elif diff > 0:
            return 'top'
        return 'center'

    def is_blocked_on_x(self, diff: int) -> bool:
        side = self._get_side_x(diff)
        if abs(self.blocked_sides.get_side(side)) > abs(diff):
            return True
        return False

    def is_blocked_on_y(self, diff: int) -> bool:
        side = self._get_side_y(diff)
        if abs(self.blocked_sides.get_side(side)) > abs(diff):
            return True
        return False

    def block_sides(self, blocked_sides: BlockedSide, block_point: 'Point') -> None:
        if blocked_sides.left:
            self.blocked_sides.left = block_point.x + self.x
        if blocked_sides.right:
            self.blocked_sides.right = block_point.x - self.x
        if blocked_sides.top:
            self.blocked_sides.top = self.y - block_point.y
        if blocked_sides.bottom:
            self.blocked_sides.bottom = block_point.y - self.y


@dataclass
class AreaMap:
    bounds: Bound
    points: Dict[Tuple[int, int], Point] = field(default_factory=dict)

    def is_out_of_bounds(self, point: Point) -> BlockedSide:
        return BlockedSide(
            left=point.x < self.bounds.left,
            right=point.x >= self.bounds.right,
            top=point.y < self.bounds.top,
            bottom=point.y >= self.bounds.bottom,

            # left=self.bounds.left - point.x,
            # right=self.bounds.right - point.x,
            # top=self.bounds.top - point.y,
            # bottom=self.bounds.bottom - point.y,
        )

    def draw(self) -> string:
        graphic = {}
        for x in range(self.bounds.left, self.bounds.right):
            for y in range(self.bounds.top, self.bounds.bottom):
                graphic[(x, y)] = '░'

        for (x, y), point in self.points.items():
            graphic[(x, y)] = point.name

        string = ''
        current_x = -1
        for (x, y), point in sorted(graphic.items()):
            if x != current_x:
                string += '\n'
                current_x = x
            string += point

        return string

    def seed(self, points: List[Point]) -> None:
        for center in points:
            self.points[(center.x, center.y)] = center


def parse_inputs(inputs: List[str]) -> List[Point]:
    res = []
    names = iter(
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØ'
        'ÙÚÛÜÝÞĀĂĄĆĈĊČĎĐĒĔĖĘĚ'
        'ĜĞĠĢĤĦĨĪĬĮİĲĴĶĹĻĽĿŁŃŅŇŊŌŎŐŒŔŖŘ'
    )
    for i, item in enumerate(inputs):
        y, x = item.split(',')
        res.append(Point(int(x), int(y), name=next(names)))
    return res


def get_area_bounds(inputs: List[Point]) -> (Point, Point):
    left = min(0, min(inputs, key=lambda point: point.x).x)
    right = max(inputs, key=lambda point: point.x).x + 1
    top = min(0, min(inputs, key=lambda point: point.y).y)
    bottom = max(inputs, key=lambda point: point.y).y + 1
    return Point(left, top, name='^'), Point(right, bottom, name='_')


def grow_area(area: AreaMap, center: Point, distance: int) -> AreaMap:
    center = center
    for x in range(center.x-distance, center.x+distance+1):
        x_diff = (Point(x, 0) - center).x
        if center.is_blocked_on_x(x_diff):
            continue
        for y in range(center.y-distance, center.y+distance+1):
            point = Point(
                x,
                y,
                owner=center,
                name=center.name.lower(),
                distance=distance,
            )

            y_diff = (point - center).y
            if center.is_blocked_on_y(y_diff):
                continue

            manhattan_distance = cityblock((center.x, center.y), (x, y))
            if manhattan_distance != distance:
                continue

            if (x, y) in area.points:
                existing_point = area.points[(x, y)]
                if manhattan_distance == distance and manhattan_distance == existing_point.distance and center.owner != existing_point.owner:
                    point.name = '.'
                    point.owner = None
                    area.points[(x, y)] = point
                print(point)
                print(area.draw())
                continue
            blocked_sides = area.is_out_of_bounds(point)
            if blocked_sides:
                center.is_infinite = True
                center.block_sides(blocked_sides, block_point=point)
                print(point)
                print(area.draw())
                continue
            if manhattan_distance == distance:
                area.points[(x, y)] = point
            print(point)
            print(area.draw())

    return area


def process(inputs: List[str]):
    points = parse_inputs(inputs)
    ic(points)
    bounds = get_area_bounds(points)
    ic(bounds)

    area = AreaMap(
        bounds=Bound(bounds[0].x, bounds[1].x, bounds[0].y, bounds[1].y),
    )

    area.seed(points)
    for i in range(1, 6):
        for point in points:
            print(f"--- Doing point: {point.name} ---")
            area = grow_area(area, center=point, distance=i)

    # ic(area)
    str_map = area.draw()
    print(str_map)

    result = None

    ic(points)
    return result


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print(result)


if __name__ == '__main__':
    main()


def test_1():
    case = """1, 1
1, 6
8, 3
3, 4
5, 5
8, 9"""
    inputs = case.split('\n')
    res = process(inputs)
    assert res == 17


def test_2():
    case = """1, 1
2, 2"""
    inputs = case.split('\n')
    res = process(inputs)
    assert res == 17


def test_3():
    case = """1, 1"""
    inputs = case.split('\n')
    res = process(inputs)
    assert res == 17
