import datetime
import re
from collections import Counter, defaultdict
from dataclasses import dataclass, field
from itertools import cycle, islice
from operator import itemgetter
from typing import Any, Dict, List, Tuple, Iterable, Set, Union

from icecream import ic


@dataclass(repr=False)
class Node:
    value: Any
    prv: 'Node' = None
    nxt: 'Node' = None

    def __repr__(self):
        return str(self.value)


@dataclass
class LinkedList:

    head: Node = None
    tail: Node = None
    counter: int = 0

    def add(self, node: Union[Any, Node]):
        if not isinstance(node, Node):
            node = Node(node)

        if self.head is not None:
            self.tail.nxt = node
            node.prv = self.tail
            self.tail = node
        else:
            self.head = node
            self.tail = node
        self.counter += 1

    def __str__(self):
        res = ''
        head = self.head
        while head is not None:
            res += str(head.value) + ', '
            head = head.nxt
        return res

    def __len__(self):
        return self.counter

    def iter_from(self, start: Node = None):
        if start is None:
            start = self.head
        node = start
        while True:
            while node is not None:
                yield node
                node = node.nxt
            node = self.head


def parse_inputs(inputs: List[str]) -> List[int]:
    return list(map(int, inputs))


def process(to_reach: int, after: int = 10):
    ll = LinkedList()
    e1 = Node(3)
    ll.add(e1)
    e2 = Node(7)
    ll.add(e2)

    while len(ll) < to_reach+after:
        il = ll.iter_from(start=e1)
        e1 = next(islice(il, e1.value+1, e1.value+2))
        il = ll.iter_from(start=e2)
        e2 = next(islice(il, e2.value+1, e2.value+2))
        res = e1.value + e2.value
        for i in str(res):
            ll.add(int(i))

    result = ''.join(
        str(i.value)
        for i
        in islice(ll.iter_from(), to_reach, to_reach+after)
    )

    return result


def main():
    result = process(to_reach=409551)
    print(result)


if __name__ == '__main__':
    main()
