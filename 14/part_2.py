import datetime
import re
from collections import Counter, defaultdict
from dataclasses import dataclass, field
from itertools import cycle, islice
from operator import itemgetter
from typing import Any, Dict, List, Tuple, Iterable, Set, Union

from icecream import ic


@dataclass(repr=False)
class Node:
    value: Any
    prv: 'Node' = None
    nxt: 'Node' = None

    def __repr__(self):
        return str(self.value)


@dataclass
class LinkedList:

    head: Node = None
    tail: Node = None
    counter: int = 0

    def add(self, node: Union[Any, Node]):
        if not isinstance(node, Node):
            node = Node(node)

        if self.head is not None:
            self.tail.nxt = node
            node.prv = self.tail
            self.tail = node
        else:
            self.head = node
            self.tail = node
        self.counter += 1

    def __str__(self):
        res = ''
        head = self.head
        while head is not None:
            res += str(head.value) + ', '
            head = head.nxt
        return res

    def __len__(self):
        return self.counter

    def iter_from(self, start: Node = None):
        if start is None:
            start = self.head
        node = start
        while True:
            while node is not None:
                yield node
                node = node.nxt
            node = self.head

    def __reversed__(self):
        node = self.tail
        while node is not None:
            yield node
            node = node.prv


def parse_inputs(inputs: List[str]) -> List[int]:
    return list(map(int, inputs))


def process(to_find: str):
    ll = LinkedList()
    e1 = Node(3)
    ll.add(e1)
    e2 = Node(7)
    ll.add(e2)

    find_len = len(to_find)
    to_find = ''.join(reversed(to_find))
    found = ''

    while found != to_find:
        il = ll.iter_from(start=e1)
        e1 = next(islice(il, e1.value+1, e1.value+2))
        il = ll.iter_from(start=e2)
        e2 = next(islice(il, e2.value+1, e2.value+2))
        res = e1.value + e2.value
        for i in str(res):
            found = ''.join(
                str(i.value)
                for i
                in islice(reversed(ll), find_len)
            )
            if found != to_find:
                ll.add(int(i))
        if not len(ll) % 100000:
            print(len(ll))

    result = len(ll) - find_len

    return result


def main():
    result = process(to_find='409551')
    print(result)


if __name__ == '__main__':
    main()
