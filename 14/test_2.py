from part_2 import process


def test_example_1():
    res = process(to_find='51589')
    assert res == 9


def test_example_2():
    res = process(to_find='01245')
    assert res == 5


def test_example_3():
    res = process(to_find='92510')
    assert res == 18


def test_example_4():
    res = process(to_find='59414')
    assert res == 2018


