from part_1 import process


def test_example_1():
    res = process(to_reach=9)
    assert res == '5158916779'


def test_example_2():
    res = process(to_reach=5)
    assert res == '0124515891'


def test_example_3():
    res = process(to_reach=18)
    assert res == '9251071085'


def test_example_4():
    res = process(to_reach=2018)
    assert res == '5941429882'


