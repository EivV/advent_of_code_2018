import datetime
import re
from collections import Counter, defaultdict
from dataclasses import dataclass, field
from operator import itemgetter
from typing import Dict, List, Tuple, Iterable, Set, Union

from icecream import ic


@dataclass
class Pots:
    pots: Dict[int, str] = field(default_factory=dict)

    def add_pot(self, pos: int, has_plant: bool) -> None:
        self.pots[pos] = has_plant

    def get_frame_for_pos(self, pos: int) -> List[Tuple[int, bool]]:
        frame = []
        for i in range(pos-2, pos+2 + 1):
            if i not in self.pots:
                self.pots[i] = False
            frame.append((i, self.pots[i]))
        return frame

    def __iter__(self):
        for pot in sorted(self.pots.items(), key=itemgetter(0)):
            yield pot

    def __len__(self):
        return len(self.pots)

    def __str__(self) -> str:
        res = ''
        for pot in self:
            vis = pot[1] and '#' or '.'
            res += vis
        return res

    @classmethod
    def from_str(cls, pots_str: str) -> 'Pots':
        pots = Pots()
        for pos, pot in enumerate(pots_str):
            pots.add_pot(pos, has_plant=pot == '#')
        return pots

    def get_min_pos(self) -> int:
        for pot in self.pots.items():
            if pot[1]:
                return pot[0]
        return min(self.pots.keys())

    def get_max_pos(self) -> int:
        for pot in reversed(list(self.pots.items())):
            if pot[1]:
                return pot[0]
        return max(self.pots.keys())


class Generationalist:

    def __init__(self, pots: Pots, notes: Dict[str, str]) -> None:
        self.pots = pots
        self.notes = notes
        self.generations = [pots]

    @property
    def start(self) -> int:
        return self.pots.get_min_pos() - 3

    @property
    def end(self) -> int:
        return self.pots.get_max_pos() + 3

    def frame_str(self, frame):
        frame_str = ''
        for point in frame:
            if point[1]:
                frame_str += '#'
            else:
                frame_str += '.'
        return frame_str

    def tick(self) -> None:
        pots = Pots()
        for pos in range(self.start, self.end+1):
            frame = self.pots.get_frame_for_pos(pos)
            frame_str = self.frame_str(frame)
            pots.add_pot(pos, has_plant=self.notes.get(frame_str, '.') == '#')
        self.pots = pots

    def live_for(self, generations: int) -> None:
        for _ in range(generations):
            self.tick()

    def total_pot_points(self) -> int:
        return sum(pot[0] for pot in self.pots if pot[1])

    def __str__(self) -> None:
        res = ''
        for pot in self.pots:
            vis = '#' if pot[1] else '.'
            res += vis
        return res


def parse_inputs(inputs: List[str]) -> Dict[str, Union[str, dict]]:
    _, initial_state = inputs[0].split(': ')
    notes = {
        position: outcome
        for position, outcome
        in (s.split(' => ') for s in inputs[2:])
    }
    return {
        'state': initial_state,
        'notes': notes,
    }


def process(inputs: List[str]):
    parsed = parse_inputs(inputs)
    pots_str_items = parsed['state']

    pots = Pots()
    for pos, content in enumerate(pots_str_items):
        pots.add_pot(pos=pos, has_plant=content == '#')

    gen = Generationalist(pots=pots, notes=parsed['notes'])
    generations_to_do = 20
    gen.live_for(generations=generations_to_do)

    return gen.total_pot_points()


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print(result)


if __name__ == '__main__':
    main()
