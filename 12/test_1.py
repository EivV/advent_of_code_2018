from part_1 import process, Pots


inputs = r"""initial state: #..#.#..##......###...###

...## => #
..#.. => #
.#... => #
.#.#. => #
.#.## => #
.##.. => #
.#### => #
#.#.# => #
#.### => #
##.#. => #
##.## => #
###.. => #
###.# => #
####. => #"""


def test_1():
    res = process(inputs.split('\n'))
    assert res == 325
