import datetime
import re
from collections import Counter, defaultdict
from operator import itemgetter
from typing import Any, Dict, List, Tuple, Iterable, Set

from icecream import ic


def parse_inputs(inputs: List[str]) -> List[dict]:
    res = []
    for item in inputs:
        parsed = re.match(
            r'\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] (.*)',
            item,
        )
        Y, M, D, h, m, text = parsed.groups()
        date = datetime.datetime(int(Y), int(M), int(D), int(h), int(m))
        res.append({
            'date': date,
            'text': text,
        })
    return res


def sorted_events(events: List[dict]):
    for event in sorted(events, key=itemgetter('date')):
        yield event


def guard_days(events: Iterable[dict]) -> List[dict]:
    res = []
    guard_id = None
    sleep_start = None
    sleep_end = None
    for e in events:
        text = e['text']
        if text.startswith('Guard'):
            guard_id = re.match(r'Guard #(\d+)', text).group(1)
        elif text.startswith('falls'):
            sleep_start = e['date']
        elif text.startswith('wakes'):
            sleep_end = e['date']

        if not any(i is None for i in (guard_id, sleep_start, sleep_end)):
            res.append({
                'guard_id': guard_id,
                'date': e['date'],
                'sleep': sleep_start.minute,
                'wake': sleep_end.minute,
            })
            sleep_start = None
            sleep_end = None
    return res


def full_info_default():
    return {
        'asleep': 0,
        'times_asleep': [],
    }


def guard_full_info(guard_days: List[dict]) -> dict:
    res = defaultdict(full_info_default)
    for day in guard_days:
        res[day['guard_id']]['asleep'] += day['wake'] - day['sleep']
        res[day['guard_id']]['times_asleep'].append(
            (day['sleep'], day['wake'])
        )
    return res


def get_guards_slept_minutes(guards_full_info: Dict[str, Any]) -> Dict[str, Any]:

    minutes_by_guard = defaultdict(lambda: defaultdict(int))
    for guard_id, guard_info in guards_full_info.items():
        for start, end in guard_info['times_asleep']:
            for i in range(start, end):
                minutes_by_guard[guard_id][i] += 1
    return minutes_by_guard


def get_max_minute_for_guards(guards_slept_minutes):
    max_minute_by_guard = defaultdict(dict)
    for guard_id, minutes_dict in guards_slept_minutes.items():
        max_minute_by_guard[guard_id] = max(
            minutes_dict.items(),
            key=itemgetter(1),
        )
    return max_minute_by_guard


def get_guard_with_max_minute(most_slept_minute_for_guards):
    max_guard_id = None
    max_minutes = -1
    max_minute = -1
    for guard_id, minute_info in most_slept_minute_for_guards.items():
        if max_minutes < minute_info[1]:
            max_minute, max_minutes = minute_info
            max_guard_id = guard_id
    return int(max_guard_id) * max_minute


def process(inputs: List[str]):
    events = parse_inputs(inputs)
    chrono_events = sorted_events(events)
    guards_info = guard_days(chrono_events)
    guards_full_info = guard_full_info(guards_info)
    guards_slept_minutes = get_guards_slept_minutes(guards_full_info)
    most_slept_minute_for_guards = get_max_minute_for_guards(guards_slept_minutes)
    result = get_guard_with_max_minute(most_slept_minute_for_guards)
    return result


def main():
    with open('2.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print('Result:', result)


if __name__ == '__main__':
    main()
