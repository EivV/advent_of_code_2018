import datetime
import re
from collections import Counter, defaultdict
from operator import itemgetter
from typing import Dict, List, Tuple, Iterable, Set

from icecream import ic


def parse_inputs(inputs: List[str]) -> List[dict]:
    res = []
    for item in inputs:
        parsed = re.match(
            r'\[(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2})\] (.*)',
            item,
        )
        Y, M, D, h, m, text = parsed.groups()
        date = datetime.datetime(int(Y), int(M), int(D), int(h), int(m))
        res.append({
            'date': date,
            'text': text,
        })
    return res


def sorted_events(events: List[dict]):
    for event in sorted(events, key=itemgetter('date')):
        yield event


def guard_days(events: Iterable[dict]) -> List[dict]:
    res = []
    guard_id = None
    sleep_start = None
    sleep_end = None
    for e in events:
        text = e['text']
        if text.startswith('Guard'):
            guard_id = re.match(r'Guard #(\d+)', text).group(1)
        elif text.startswith('falls'):
            sleep_start = e['date']
        elif text.startswith('wakes'):
            sleep_end = e['date']

        if not any(i is None for i in (guard_id, sleep_start, sleep_end)):
            res.append({
                'guard_id': guard_id,
                'date': e['date'],
                'sleep': sleep_start.minute,
                'wake': sleep_end.minute,
            })
            sleep_start = None
            sleep_end = None
    return res


def full_info_default():
    return {
        'asleep': 0,
        'times_asleep': [],
    }


def guard_full_info(guard_days: List[dict]) -> dict:
    res = defaultdict(full_info_default)
    for day in guard_days:
        res[day['guard_id']]['asleep'] += day['wake'] - day['sleep']
        res[day['guard_id']]['times_asleep'].append(
            (day['sleep'], day['wake'])
        )
    return res


def get_most_sleepy_guard_id(info: dict) -> str:
    most_slept = 0
    sleepy_guard = None
    for guard, i in info.items():
        if i['asleep'] > most_slept:
            most_slept = i['asleep']
            sleepy_guard = guard
    return sleepy_guard


def get_overlaps(guard_info: dict) -> List[Set[int]]:
    times = guard_info['times_asleep']
    overlaps = []
    for i, time in enumerate(times):
        start, end = time
        range_1 = range(start, end)
        for other_time in times[i+1:]:
            range_2 = range(*other_time)
            overlaps.append(set(range_1).intersection(range_2))
    return overlaps


def process(inputs: List[str]):
    events = parse_inputs(inputs)
    chrono_events = sorted_events(events)
    guards_info = guard_days(chrono_events)
    guards_full_info = guard_full_info(guards_info)
    most_sleepy_guard_id = get_most_sleepy_guard_id(guards_full_info)
    all_overlaps = get_overlaps(guards_full_info[most_sleepy_guard_id])
    most_common_minutes = Counter(l for i in all_overlaps for l in i)
    the_minute = max(most_common_minutes.items(), key=itemgetter(1))[0]
    result = int(most_sleepy_guard_id) * the_minute
    return result


def main():
    with open('1.txt', 'r') as f:
        file = f.read()
    inputs = file.split('\n')

    result = process(inputs[:-1])
    print(result)


if __name__ == '__main__':
    main()


def test_1():
    case = """[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up"""
    inputs = case.split('\n')
    res = process(inputs)
    assert res == 240


def test_2():
    main()
